package javaapplication1;
import java.util.Scanner;
/**
 *
 * @author informatics
 */
public class JavaApplication1 {
    private static String username;
    private static String password;
    
    public static void showLogin(){
        System.out.println("Login");
    }
    public static void inputLogin(){
        Scanner kb = new Scanner(System.in);
        
        System.out.print("Username : ");
        username = kb.next();
        System.out.print("Password : ");
        password = kb.next();
    }
    
    public static void main(String[] args) {
        while(true){
            showLogin();
            inputLogin();
            if(!isUsernameCorrect(username)){
                showLoginError();
                continue;
            }
            if(!isPasswordCorrect(password)){
                showLoginError();
                continue;
            }
            break;
        }
        
    
}
    
    private static boolean isUsernameCorrect(String username){
        return true;
    }
    
    private static boolean isPasswordCorrect(String password){
        return true;
    }
    
    private static void showLoginError(){
        System.out.println("username or password incorrect");
    }
    
    
}
